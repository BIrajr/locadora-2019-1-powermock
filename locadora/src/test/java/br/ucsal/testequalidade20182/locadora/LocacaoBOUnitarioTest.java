package br.ucsal.testequalidade20182.locadora;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import br.ucsal.testequalidade20182.locadora.business.LocacaoBO;
import br.ucsal.testequalidade20182.locadora.dominio.Cliente;
import br.ucsal.testequalidade20182.locadora.dominio.Locacao;
import br.ucsal.testequalidade20182.locadora.dominio.Veiculo;
import br.ucsal.testequalidade20182.locadora.persistence.ClienteDAO;
import br.ucsal.testequalidade20182.locadora.persistence.LocacaoDAO;
import br.ucsal.testequalidade20182.locadora.persistence.VeiculoDAO;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ LocacaoBO.class })
public class LocacaoBOUnitarioTest {

	/**
	 * Locar, para um cliente cadastrado, um veículo disponível. Método: public
	 * static Integer locarVeiculos(String cpfCliente, List<String> placas, Date
	 * dataLocacao, Integer quantidadeDiasLocacao) throws
	 * ClienteNaoEncontradoException, VeiculoNaoEncontradoException,
	 * VeiculoNaoDisponivelException, CampoObrigatorioNaoInformado {
	 *
	 * Observações: lembre-se de mocar os métodos necessários nas classes
	 * ClienteDAO, VeiculoDAO e LocacaoDAO.
	 * 
	 * @throws Exception
	 */
	@Test
	public void locarClienteCadastradoUmVeiculoDisponivel() throws Exception {
		Cliente cliente1 = new Cliente("123", "Eli", "456");
		Veiculo veiculo1 = new Veiculo("opx", 1998, null, 20.0);
		Locacao locacao1 = new Locacao(cliente1, null, null, 2);

		ClienteDAO clienteDAO1 = new ClienteDAO();
		ClienteDAO spy1 = PowerMockito.spy(clienteDAO1);
		PowerMockito.mockStatic(ClienteDAO.class);

		VeiculoDAO veiculoDAO = new VeiculoDAO();
		VeiculoDAO spy2 = PowerMockito.spy(veiculoDAO);
		PowerMockito.mockStatic(VeiculoDAO.class);

		LocacaoDAO locacaoDAO = new LocacaoDAO();
		LocacaoDAO spy3 = PowerMockito.spy(locacaoDAO);
		PowerMockito.mockStatic(LocacaoDAO.class);

		LocacaoBO locacaoBO = new LocacaoBO();
		LocacaoBO spy4 = PowerMockito.spy(locacaoBO);

		PowerMockito.mockStatic(LocacaoBO.class);
		PowerMockito.when(ClienteDAO.obterPorCpf("123")).thenReturn(cliente1);
		PowerMockito.when(VeiculoDAO.obterPorPlaca("opx")).thenReturn(veiculo1);
		PowerMockito.when(LocacaoDAO.obterPorNumeroContrato(1)).thenReturn(locacao1);
		
		PowerMockito.verifyStatic();
		
		
		int resultadoEsperado = 1;
		int resultadoAtual = spy4.locarVeiculos("123", null, null, 2);
		Assert.assertEquals(resultadoEsperado, resultadoAtual);
		
		
	}
}
