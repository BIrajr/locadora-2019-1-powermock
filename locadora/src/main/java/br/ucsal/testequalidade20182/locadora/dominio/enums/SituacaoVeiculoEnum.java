package br.ucsal.testequalidade20182.locadora.dominio.enums;

public enum SituacaoVeiculoEnum {
	DISPONIVEL, MANUTENCAO, LOCADO;
}
